<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_roles')->delete();
        
        \DB::table('admin_roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Administrator',
                'slug' => 'administrator',
                'created_at' => '2022-07-30 13:01:13',
                'updated_at' => '2022-07-30 13:01:13',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Guru TK',
                'slug' => 'guru',
                'created_at' => '2022-07-30 13:21:40',
                'updated_at' => '2022-07-30 13:21:40',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Orang Tua Siswa',
                'slug' => 'ortu-siswa',
                'created_at' => '2022-07-30 13:22:13',
                'updated_at' => '2022-07-30 13:22:13',
            ),
        ));
        
        
    }
}