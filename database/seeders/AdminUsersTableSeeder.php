<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_users')->delete();
        
        \DB::table('admin_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'admin',
                'password' => '$2y$10$tpbYojq3Hgw5cwzGK2QwNO29UqspVP2.Et2OvHpLe/kvATDj63kMa',
                'name' => 'Administrator',
                'avatar' => NULL,
                'remember_token' => 'HR5VEntLiduPCQSqdjIGnhX9wVfZ9g9ptNyuOMoXdgHE9Zs6IktE9Tr09qF6',
                'created_at' => '2022-07-30 13:01:13',
                'updated_at' => '2022-07-30 13:01:13',
                'opsi' => '',
                'nis' => '',
                'no_telepon' => '',
            ),
            1 => 
            array (
                'id' => 2,
                'username' => 'guru',
                'password' => '$2y$10$rCR5isn50LRUPFIhV1dCeuQw585Fjld.WmNUYQtnhf/mh5d1PORsq',
                'name' => 'Fajar Guru',
                'avatar' => NULL,
                'remember_token' => 'k2HEU8uAqVgWRUxcx9XrWJJsx4uNnviaxy3iH8FC0Nffq8rxjg4vKSJ4VrkP',
                'created_at' => '2022-07-30 13:20:25',
                'updated_at' => '2022-07-31 04:58:04',
                'opsi' => '',
                'nis' => '',
                'no_telepon' => '',
            ),
            2 => 
            array (
                'id' => 3,
                'username' => 'orang-tua',
                'password' => '$2y$10$N8XBe89eB5O.ch9So/A7R./fmVdWJ8M3tADopBEb2uu70us/QxtgW',
                'name' => 'Rahman',
                'avatar' => NULL,
                'remember_token' => 'ohfxmzBbvgR1be1XZ1yiVA1TrhrRxcxrTlUxfQ2OPI7llPUHRZu4A3dSsiPk',
                'created_at' => '2022-07-30 13:20:50',
                'updated_at' => '2022-07-30 13:23:09',
                'opsi' => 'Orang Tua',
                'nis' => '076778878',
                'no_telepon' => '0898986',
            ),
            3 => 
            array (
                'id' => 4,
                'username' => 'amir',
                'password' => '$2y$10$m2wefqUDD9mvAcF6Ai9kdukNMhH9argM.JQ7V93tbCitGznOPayFS',
                'name' => 'Amir',
                'avatar' => NULL,
                'remember_token' => '9c61zIGPagdZaX4DrksHxArfnPU5fzCPfKxx03lNhW7YVuDEZJGhDeBiscm4',
                'created_at' => '2022-07-31 05:30:05',
                'updated_at' => '2022-07-31 05:30:05',
                'opsi' => 'Orang Tua',
                'nis' => '089778712',
                'no_telepon' => '089876567576',
            ),
        ));
        
        
    }
}