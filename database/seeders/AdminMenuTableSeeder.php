<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminMenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_menu')->delete();
        
        \DB::table('admin_menu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Dashboard',
                'icon' => 'fa-bar-chart',
                'uri' => '/',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 0,
                'order' => 9,
                'title' => 'Admin',
                'icon' => 'fa-tasks',
                'uri' => '',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2022-07-31 04:45:00',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 2,
                'order' => 10,
                'title' => 'Users',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2022-07-31 04:45:00',
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 2,
                'order' => 11,
                'title' => 'Roles',
                'icon' => 'fa-user',
                'uri' => 'auth/roles',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2022-07-31 04:45:00',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 2,
                'order' => 12,
                'title' => 'Permission',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2022-07-31 04:45:00',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 2,
                'order' => 13,
                'title' => 'Menu',
                'icon' => 'fa-bars',
                'uri' => 'auth/menu',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2022-07-31 04:45:00',
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 2,
                'order' => 14,
                'title' => 'Operation log',
                'icon' => 'fa-history',
                'uri' => 'auth/logs',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2022-07-31 04:45:00',
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 0,
                'order' => 3,
                'title' => 'Data Master',
                'icon' => 'fa-database',
                'uri' => '#',
                'permission' => NULL,
                'created_at' => '2022-07-30 13:26:51',
                'updated_at' => '2022-07-31 04:45:00',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 8,
                'order' => 5,
                'title' => 'Data Indikator',
                'icon' => 'fa-tags',
                'uri' => '/master/indikator',
                'permission' => 'indikator-index',
                'created_at' => '2022-07-30 13:28:31',
                'updated_at' => '2022-07-31 04:45:00',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 8,
                'order' => 4,
                'title' => 'Data Kategori Aspek',
                'icon' => 'fa-archive',
                'uri' => '/master/kategori-aspek',
                'permission' => 'kategori-aspek-index',
                'created_at' => '2022-07-30 13:29:18',
                'updated_at' => '2022-07-31 04:45:00',
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 0,
                'order' => 7,
                'title' => 'Penilaian Siswa',
                'icon' => 'fa-users',
                'uri' => '/penilaian-siswa',
                'permission' => NULL,
                'created_at' => '2022-07-30 13:29:53',
                'updated_at' => '2022-07-31 04:45:00',
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 0,
                'order' => 6,
                'title' => 'Raport TK',
                'icon' => 'fa-book',
                'uri' => '/raport-tk',
                'permission' => NULL,
                'created_at' => '2022-07-30 13:38:57',
                'updated_at' => '2022-07-31 04:45:00',
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 0,
                'order' => 2,
                'title' => 'Siswa TK',
                'icon' => 'fa-user-plus',
                'uri' => '/siswa-tk',
                'permission' => NULL,
                'created_at' => '2022-07-30 14:42:21',
                'updated_at' => '2022-07-31 04:45:00',
            ),
            13 => 
            array (
                'id' => 17,
                'parent_id' => 0,
                'order' => 8,
                'title' => 'Lihat Raport Anak',
                'icon' => 'fa-book',
                'uri' => '/lihat-raport-anak',
                'permission' => 'lihat-raport-anak-index',
                'created_at' => '2022-07-31 04:44:55',
                'updated_at' => '2022-07-31 04:48:52',
            ),
        ));
        
        
    }
}