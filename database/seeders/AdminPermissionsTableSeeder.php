<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_permissions')->delete();
        
        \DB::table('admin_permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'All permission',
                'slug' => '*',
                'http_method' => '',
                'http_path' => '*',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Dashboard',
                'slug' => 'dashboard',
                'http_method' => 'GET',
                'http_path' => '/',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Login',
                'slug' => 'auth.login',
                'http_method' => '',
                'http_path' => '/auth/login
/auth/logout',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'User setting',
                'slug' => 'auth.setting',
                'http_method' => 'GET,PUT',
                'http_path' => '/auth/setting',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Auth management',
                'slug' => 'auth.management',
                'http_method' => '',
                'http_path' => '/auth/roles
/auth/permissions
/auth/menu
/auth/logs',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 10,
                'name' => 'Index Siswa',
                'slug' => 'siswa-tk',
                'http_method' => 'GET',
                'http_path' => '/siswa-tk/',
                'created_at' => '2022-07-31 04:02:44',
                'updated_at' => '2022-07-31 04:02:44',
            ),
            6 => 
            array (
                'id' => 11,
                'name' => 'Siswa TK Create',
                'slug' => 'siswa-tk-create',
                'http_method' => 'GET',
                'http_path' => '/siswa-tk/create',
                'created_at' => '2022-07-31 04:04:14',
                'updated_at' => '2022-07-31 04:05:01',
            ),
            7 => 
            array (
                'id' => 12,
                'name' => 'Siswa TK Post',
                'slug' => 'siswa-tk-post',
                'http_method' => 'POST',
                'http_path' => '/siswa-tk/',
                'created_at' => '2022-07-31 04:05:36',
                'updated_at' => '2022-07-31 04:05:36',
            ),
            8 => 
            array (
                'id' => 13,
                'name' => 'Siswa TK Edit',
                'slug' => 'siswa-tk-edit',
                'http_method' => 'GET',
                'http_path' => '/siswa-tk/*/edit',
                'created_at' => '2022-07-31 04:06:44',
                'updated_at' => '2022-07-31 04:06:44',
            ),
            9 => 
            array (
                'id' => 15,
                'name' => 'Siswa TK Update',
                'slug' => 'siswa-tk-update',
                'http_method' => 'PUT',
                'http_path' => '/siswa-tk/*',
                'created_at' => '2022-07-31 04:07:36',
                'updated_at' => '2022-07-31 04:07:36',
            ),
            10 => 
            array (
                'id' => 16,
                'name' => 'Siswa TK View',
                'slug' => 'siswa-tk-view',
                'http_method' => 'GET',
                'http_path' => '/siswa-tk/*',
                'created_at' => '2022-07-31 04:07:55',
                'updated_at' => '2022-07-31 04:07:55',
            ),
            11 => 
            array (
                'id' => 17,
                'name' => 'Siswa TK Delete',
                'slug' => 'siswa-tk-delete',
                'http_method' => 'DELETE',
                'http_path' => '/siswa-tk/*',
                'created_at' => '2022-07-31 04:08:21',
                'updated_at' => '2022-07-31 04:08:21',
            ),
            12 => 
            array (
                'id' => 18,
                'name' => 'Raport TK',
                'slug' => 'raport-tk',
                'http_method' => 'GET',
                'http_path' => '/raport-tk/',
                'created_at' => '2022-07-31 04:09:52',
                'updated_at' => '2022-07-31 04:09:52',
            ),
            13 => 
            array (
                'id' => 19,
                'name' => 'Raport TK Create',
                'slug' => 'raport-tk-create',
                'http_method' => 'GET',
                'http_path' => '/raport-tk/create',
                'created_at' => '2022-07-31 04:10:11',
                'updated_at' => '2022-07-31 04:10:11',
            ),
            14 => 
            array (
                'id' => 20,
                'name' => 'Raport Tk Post',
                'slug' => 'raport-tk-post',
                'http_method' => 'POST',
                'http_path' => '/raport-tk/',
                'created_at' => '2022-07-31 04:10:28',
                'updated_at' => '2022-07-31 04:10:28',
            ),
            15 => 
            array (
                'id' => 21,
                'name' => 'Raport TK Edit',
                'slug' => 'raport-tk-edit',
                'http_method' => 'GET',
                'http_path' => '/raport-tk/*/edit',
                'created_at' => '2022-07-31 04:10:56',
                'updated_at' => '2022-07-31 04:10:56',
            ),
            16 => 
            array (
                'id' => 22,
                'name' => 'Raport TK Update',
                'slug' => 'raport-tk-update',
                'http_method' => 'PUT',
                'http_path' => '/raport-tk/*',
                'created_at' => '2022-07-31 04:11:13',
                'updated_at' => '2022-07-31 04:11:13',
            ),
            17 => 
            array (
                'id' => 23,
                'name' => 'Raport TK VIew',
                'slug' => 'raport-tk-view',
                'http_method' => 'GET',
                'http_path' => '/raport-tk/*',
                'created_at' => '2022-07-31 04:11:43',
                'updated_at' => '2022-07-31 04:11:43',
            ),
            18 => 
            array (
                'id' => 24,
                'name' => 'Raport TK Delete',
                'slug' => 'raport-tk-delete',
                'http_method' => 'DELETE',
                'http_path' => '/raport-tk/*',
                'created_at' => '2022-07-31 04:12:10',
                'updated_at' => '2022-07-31 04:12:10',
            ),
            19 => 
            array (
                'id' => 25,
                'name' => 'Penilaian Siswa Index',
                'slug' => 'penilaian-siswa-index',
                'http_method' => 'GET',
                'http_path' => '/penilaian-siswa/',
                'created_at' => '2022-07-31 04:12:51',
                'updated_at' => '2022-07-31 04:12:51',
            ),
            20 => 
            array (
                'id' => 26,
                'name' => 'Penilaian Siswa Create',
                'slug' => 'penilaian-siswa-create',
                'http_method' => 'GET',
                'http_path' => '/penilaian-siswa/create',
                'created_at' => '2022-07-31 04:13:06',
                'updated_at' => '2022-07-31 04:13:06',
            ),
            21 => 
            array (
                'id' => 27,
                'name' => 'Penilaian Siswa Edit',
                'slug' => 'penilaian-siswa-edit',
                'http_method' => 'GET',
                'http_path' => '/penilaian-siswa/*/edit',
                'created_at' => '2022-07-31 04:13:28',
                'updated_at' => '2022-07-31 04:13:28',
            ),
            22 => 
            array (
                'id' => 28,
                'name' => 'Penilaian Siswa Update',
                'slug' => 'penilaian-siswa-update',
                'http_method' => 'PUT',
                'http_path' => '/penilaian-siswa/*',
                'created_at' => '2022-07-31 04:13:42',
                'updated_at' => '2022-07-31 04:13:42',
            ),
            23 => 
            array (
                'id' => 29,
                'name' => 'Penilaian Siswa View',
                'slug' => 'penilaian-siswa-view',
                'http_method' => 'GET',
                'http_path' => '/penilaian-siswa/*',
                'created_at' => '2022-07-31 04:13:56',
                'updated_at' => '2022-07-31 04:13:56',
            ),
            24 => 
            array (
                'id' => 30,
                'name' => 'Penilaian Siswa Delete',
                'slug' => 'penilaian-siswa-delete',
                'http_method' => 'DELETE',
                'http_path' => '/penilaian-siswa/*',
                'created_at' => '2022-07-31 04:14:11',
                'updated_at' => '2022-07-31 04:14:11',
            ),
            25 => 
            array (
                'id' => 31,
                'name' => 'Kategori Aspek Index',
                'slug' => 'kategori-aspek-index',
                'http_method' => 'GET',
                'http_path' => '/master/kategori-aspek/',
                'created_at' => '2022-07-31 04:16:00',
                'updated_at' => '2022-07-31 04:18:59',
            ),
            26 => 
            array (
                'id' => 32,
                'name' => 'Kategori Aspek Create',
                'slug' => 'kategori-aspek-create',
                'http_method' => 'GET',
                'http_path' => '/master/kategori-aspek/create',
                'created_at' => '2022-07-31 04:16:25',
                'updated_at' => '2022-07-31 04:19:10',
            ),
            27 => 
            array (
                'id' => 33,
                'name' => 'Kategori Aspek Store',
                'slug' => 'kategori-aspek-store',
                'http_method' => 'POST',
                'http_path' => '/master/kategori-aspek/',
                'created_at' => '2022-07-31 04:16:49',
                'updated_at' => '2022-07-31 04:19:17',
            ),
            28 => 
            array (
                'id' => 34,
                'name' => 'Kategori Aspek Edit',
                'slug' => 'kategori-aspek-edit',
                'http_method' => 'GET',
                'http_path' => '/master/kategori-aspek/*/edit',
                'created_at' => '2022-07-31 04:17:07',
                'updated_at' => '2022-07-31 04:19:24',
            ),
            29 => 
            array (
                'id' => 35,
                'name' => 'Kategori Aspek Update',
                'slug' => 'kategori-aspek-update',
                'http_method' => 'PUT',
                'http_path' => '/master/kategori-aspek/*',
                'created_at' => '2022-07-31 04:17:25',
                'updated_at' => '2022-07-31 04:19:31',
            ),
            30 => 
            array (
                'id' => 36,
                'name' => 'Kategori Aspek View',
                'slug' => 'kategori-aspek-view',
                'http_method' => 'GET',
                'http_path' => '/master/kategori-aspek/*',
                'created_at' => '2022-07-31 04:17:43',
                'updated_at' => '2022-07-31 04:19:38',
            ),
            31 => 
            array (
                'id' => 37,
                'name' => 'Kategori Aspek Delete',
                'slug' => 'kategori-aspek-delete',
                'http_method' => 'DELETE',
                'http_path' => '/master/kategori-aspek/*',
                'created_at' => '2022-07-31 04:17:58',
                'updated_at' => '2022-07-31 04:19:44',
            ),
            32 => 
            array (
                'id' => 38,
                'name' => 'Indikator Index',
                'slug' => 'indikator-index',
                'http_method' => 'GET',
                'http_path' => '/master/indikator/',
                'created_at' => '2022-07-31 04:20:24',
                'updated_at' => '2022-07-31 04:20:24',
            ),
            33 => 
            array (
                'id' => 39,
                'name' => 'Indikator Create',
                'slug' => 'indikator-create',
                'http_method' => 'GET',
                'http_path' => '/master/indikator/create',
                'created_at' => '2022-07-31 04:20:42',
                'updated_at' => '2022-07-31 04:20:42',
            ),
            34 => 
            array (
                'id' => 40,
                'name' => 'Indikator Store',
                'slug' => 'indikator-store',
                'http_method' => 'POST',
                'http_path' => '/master/indikator/',
                'created_at' => '2022-07-31 04:21:00',
                'updated_at' => '2022-07-31 04:21:00',
            ),
            35 => 
            array (
                'id' => 41,
                'name' => 'Indikator Edit',
                'slug' => 'indikator-edit',
                'http_method' => 'GET',
                'http_path' => '/master/indikator/*/edit',
                'created_at' => '2022-07-31 04:21:22',
                'updated_at' => '2022-07-31 04:21:22',
            ),
            36 => 
            array (
                'id' => 42,
                'name' => 'Indikator Update',
                'slug' => 'indikator-update',
                'http_method' => 'PUT',
                'http_path' => '/master/indikator/*',
                'created_at' => '2022-07-31 04:21:41',
                'updated_at' => '2022-07-31 04:21:41',
            ),
            37 => 
            array (
                'id' => 43,
                'name' => 'Indikator View',
                'slug' => 'indikator-view',
                'http_method' => 'GET',
                'http_path' => '/master/indikator/*',
                'created_at' => '2022-07-31 04:22:00',
                'updated_at' => '2022-07-31 04:22:00',
            ),
            38 => 
            array (
                'id' => 44,
                'name' => 'Indikator Delete',
                'slug' => 'indikator-delete',
                'http_method' => 'DELETE',
                'http_path' => '/master/indikator/*',
                'created_at' => '2022-07-31 04:22:14',
                'updated_at' => '2022-07-31 04:22:14',
            ),
            39 => 
            array (
                'id' => 45,
                'name' => 'Lihat Raport Anak Index',
                'slug' => 'lihat-raport-anak-index',
                'http_method' => 'GET',
                'http_path' => '/lihat-raport-anak/',
                'created_at' => '2022-07-31 04:47:59',
                'updated_at' => '2022-07-31 04:47:59',
            ),
            40 => 
            array (
                'id' => 46,
                'name' => 'Lihat Raport Anak View',
                'slug' => 'lihat-raport-anak-view',
                'http_method' => 'GET',
                'http_path' => '/lihat-raport-anak/*',
                'created_at' => '2022-07-31 04:48:22',
                'updated_at' => '2022-07-31 04:48:22',
            ),
            41 => 
            array (
                'id' => 47,
                'name' => 'Penilaian Siswa Store',
                'slug' => 'penilaian-siswa-store',
                'http_method' => 'POST',
                'http_path' => '/penilaian-siswa/',
                'created_at' => '2022-07-31 05:00:11',
                'updated_at' => '2022-07-31 05:00:11',
            ),
        ));
        
        
    }
}