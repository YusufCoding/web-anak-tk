<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');

    $router->group([
        'prefix' => 'master',
        'as'     => 'master.'
    ], function(Router $master) {
        $master->resource('kategori-aspek', KategoriAspekController::class);
        $master->resource('indikator', IndikatorController::class);
    });

    $router->resource('siswa-tk', SiswaTKController::class);

    $router->resource('raport-tk', RaportTKController::class);
    $router->resource('penilaian-siswa', PenilaianSiswaController::class);
    $router->resource('ortu-siswa', OrtuSiswaController::class);

    $router->resource('lihat-raport-anak', LihatRaportAnakController::class);
});
