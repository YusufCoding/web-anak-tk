<?php

namespace App\Admin\Controllers;

use App\Models\RaportTK;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

class LihatRaportAnakController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Raport Anak';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RaportTK());

        $grid->model()->where('nis', Admin::user()->nis);

        $grid->column('siswatk.nama', __('Nama Siswa'));
        $grid->column('nis', __('Nis'));
        $grid->column('semester', __('Semester'));
        $grid->column('by.username', __('Dibuat Oleh'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RaportTK::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nis', __('Nis'));
        $show->field('semester', __('Semester'));
        $show->field('created_by', __('Created by'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RaportTK());

        $form->text('nis', __('Nis'));
        $form->text('semester', __('Semester'));
        $form->number('created_by', __('Created by'));

        return $form;
    }
}
