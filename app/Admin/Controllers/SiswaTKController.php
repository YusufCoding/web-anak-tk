<?php

namespace App\Admin\Controllers;

use App\Models\SiswaTK;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SiswaTKController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Siswa TK';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SiswaTK());

        $grid->column('nama', __('Nama'));
        $grid->column('nis', __('Nis'));
        $grid->column('jkl', __('Jenis Kelamin'));
        $grid->column('tgl_lahir', __('Tgl lahir'));
        $grid->column('tgl_masuk', __('Tgl masuk'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SiswaTK::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nama', __('Nama'));
        $show->field('nis', __('Nis'));
        $show->field('agama', __('Agama'));
        $show->field('jkl', __('Jkl'));
        $show->field('tempat_lahir', __('Tempat lahir'));
        $show->field('tgl_lahir', __('Tgl lahir'));
        $show->field('tgl_masuk', __('Tgl masuk'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SiswaTK());

        $form->text('nama', __('Nama'));
        $form->text('nis', __('Nis'));
        $form->select('agama', __('Agama'))->options([
            "Islam" => "Islam",
            "Kristen Protestan" => "Kristen Protestan",
            "Kristen Katolik" => "Kristen Katolik",
            "Hindu" => "Hindu",
            "Buddha" => "Buddha",
            "Khonghucu" => "Khonghucu",
        ]);
        $form->select('jkl', __('Jenis Kelamin'))
                ->options([
                    "Laki-Laki" => "Laki-Laki",
                    "Perempuan" => "Perempuan"
                ]);
        $form->text('tempat_lahir', __('Tempat lahir'));
        $form->date('tgl_lahir', __('Tgl lahir'))->default(date('Y-m-d'));
        $form->date('tgl_masuk', __('Tgl masuk'))->default(date('Y-m-d'));

        return $form;
    }
}
