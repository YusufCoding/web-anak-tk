<?php

namespace App\Admin\Controllers;

use App\Models\Indikator;
use App\Models\KategoriAspek;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class IndikatorController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Indikator';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Indikator());

        $grid->column('nama_indikator', __('Nama indikator'));
        $grid->column('aspek.nama_kategori_aspek', __('Kategori aspek'));
        $grid->column('isi_indikator', __('Isi indikator'))->display(function() {
            return htmlspecialchars($this->isi_indikator);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Indikator::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nama_indikator', __('Nama indikator'));
        $show->field('id_kategori_aspek', __('Id kategori aspek'));
        $show->field('isi_indikator', __('Isi indikator'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Indikator());

        $form->text('nama_indikator', __('Nama indikator'));
        $form->select('id_kategori_aspek', __('Kategori aspek'))
                ->options(KategoriAspek::pluck('nama_kategori_aspek', 'id'));
        $form->ckeditor('isi_indikator', __('Isi indikator'));

        return $form;
    }
}
