<?php

namespace App\Admin\Controllers;

use App\Models\RaportTK;
use App\Models\SiswaTK;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RaportTKController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Raport TK';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RaportTK());

        $grid->column('siswatk.nis', __('NIS Siswa'));
        $grid->column('siswatk.nama', __('Nama Siswa'));
        $grid->column('semester', __('Semester'));
        $grid->column('by.username', __('Dibuat Oleh'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RaportTK::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nis', __('Nis'));
        $show->field('semester', __('Semester'));
        $show->field('created_by', __('Created by'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RaportTK());

        $form->select('nis', __('NIS'))
                ->options(SiswaTK::pluck('nis', 'nis'));
        $form->text('semester', __('Semester'));
        $form->hidden('created_by', __('Dibuat Oleh'))->default(Admin::user()->id);

        return $form;
    }
}
