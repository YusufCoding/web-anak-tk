<?php

namespace App\Admin\Controllers;

use App\Models\PenilaianSiswa;
use App\Models\RaportTK;
use App\Models\SiswaTK;
use App\Models\KategoriAspek;
use App\Models\Indikator;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PenilaianSiswaController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Penilaian Siswa';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PenilaianSiswa());

        $grid->column('raport.id', __('Raport Siswa'))->display(function() {
            $siswatk = SiswaTK::where('nis', $this->raport->nis)->first();
            return $siswatk->nama;
        });
        $grid->column('raport.nis', __('NIS Siswa')); 
        $grid->column('aspek.nama_kategori_aspek', __('Kategori aspek')); 
        $grid->column('nilai', __('Nilai'));
        $grid->column('keterangan', __('Keterangan'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PenilaianSiswa::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('id_raport', __('Id raport'));
        $show->field('id_kategori_aspek', __('Id kategori aspek'));
        $show->field('id_indikator', __('Id indikator'));
        $show->field('nilai', __('Nilai'));
        $show->field('keterangan', __('Keterangan'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PenilaianSiswa());

        $form->select('id_raport', __('Raport Siswa'))
                ->options(RaportTK::pluck('nis', 'id'));
        $form->select('id_kategori_aspek', __('Kategori aspek'))
                ->options(KategoriAspek::pluck('nama_kategori_aspek', 'id'));
        $form->select('id_indikator', __('Indikator'))
                ->options(Indikator::pluck('nama_indikator', 'id'));
        $form->text('nilai', __('Nilai'));
        $form->textarea('keterangan', __('Keterangan'));
        $form->hidden('created_by', __('Dibuat Oleh'))->default(Admin::user()->id);

        return $form;
    }
}
