<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nama_indikator
 * @property integer $id_kategori_aspek
 * @property string $isi_indikator
 * @property string $created_at
 * @property string $updated_at
 */
class Indikator extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'indikator';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nama_indikator', 'id_kategori_aspek', 'isi_indikator', 'created_at', 'updated_at'];

    public function aspek()
    {
        return $this->belongsTo(KategoriAspek::class, 'id_kategori_aspek', 'id');
    }
}
