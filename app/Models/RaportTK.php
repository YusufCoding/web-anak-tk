<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nis
 * @property string $semester
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class RaportTK extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'raport_tk';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nis', 'semester', 'created_by', 'created_at', 'updated_at'];

    public function by()
    {
        return $this->belongsTo(Guru::class, 'created_by', 'id');
    }

    public function siswatk()
    {
        return $this->belongsTo(SiswaTK::class, 'nis', 'nis');
    }
}
