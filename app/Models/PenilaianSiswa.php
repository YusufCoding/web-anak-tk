<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_raport
 * @property integer $id_kategori_aspek
 * @property integer $id_indikator
 * @property string $nilai
 * @property string $keterangan
 * @property string $created_at
 * @property string $updated_at
 */
class PenilaianSiswa extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'penilaian_siswa';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_raport', 'id_kategori_aspek', 'id_indikator', 'nilai', 'keterangan', 'created_at', 'updated_at'];

    public function raport()
    {
        return $this->belongsTo(RaportTK::class, 'id_raport', 'id');
    }

    public function aspek()
    {
        return $this->belongsTo(KategoriAspek::class, 'id_kategori_aspek', 'id');
    }

    public function indikator()
    {
        return $this->belongsTo(Indikator::class, 'id_indikator', 'id');
    }

    public function by()
    {
        return $this->belongsTo(Guru::class, 'created_by', 'id');
    }
}
