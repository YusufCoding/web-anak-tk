<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $nama
 * @property string $nis
 * @property string $agama
 * @property string $jkl
 * @property string $tempat_lahir
 * @property string $tgl_lahir
 * @property string $tgl_masuk
 * @property string $created_at
 * @property string $updated_at
 */
class SiswaTK extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'siswa_tk';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['nama', 'nis', 'agama', 'jkl', 'tempat_lahir', 'tgl_lahir', 'tgl_masuk', 'created_at', 'updated_at'];
}
